@extends('main')

@section('title', 'Index')

@section('main')
    <h2>Entries</h2>
    @foreach ($entries as $entry)
    <article>
        <strong>
            {{ $entry->name }}
            @if ($entry->email !== "")
                &lt;{{ $entry->email }}&gt;
            @endif
        </strong>
        <br>
        <p>{{ $entry->message }}</p>
    </article>
    @endforeach
    {{ $entries->links() }}
    <h2>Add your own entry</h2>
    <form method="post" action="{{ route('store') }}">
        {{ csrf_field() }}
        <fieldset>
            <input name="name" type="text" placeholder="Your name" required />
            <input name="email" type="email" placeholder="Your email address" />
            <br>
            <textarea name="body" placeholder="Your message" style="width: 500px; height: 200px;" required></textarea>
            <br>
            <button type="submit">Add your entry</button>
        </fieldset>
    </form>
@endsection
