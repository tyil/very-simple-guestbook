<!doctype html5>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{{ config('app.name') }} - @yield('title')</title>
    </head>
    <body>
        @section('body')
            <header>
                <h1>{{ config('app.name') }} - @yield('title')</h1>
            </header>
            <main>
                @yield('main')
            </main>
            <footer>
            </footer>
        @show
    </body>
</html>
