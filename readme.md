# Guestbook

A small PHP application.

## Setup

To get this application to work, you'll need to clone the sources, install
dependencies and serve it with a webserver. This can be the built-in PHP
webserver, if you don't want to configure a full webserver. The following list
of commands should take care of all these steps.

```sh
cd "$(mktemp -d)"
git clone https://gitlab.com/tyil/guestbook .
cp .env.example .env
touch database/database.sqlite
composer install
npm install
php artisan migrate
php artisan serve
```
