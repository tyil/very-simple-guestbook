<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Http\Requests\EntryStoreRequest;
use DB;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = Entry::paginate(30);

        return view('index', [
            'entries' => $entries,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntryStoreRequest $request)
    {
        try {
            DB::beginTransaction();

            Entry::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'message' => $request->get('body'),
            ]);

            DB::commit();

            return redirect(route('index'));
        } catch (Exception $e) {
            DB::rollback();

            return back()->withInput();
        }
    }
}
